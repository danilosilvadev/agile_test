/* eslint-disable */

import React from 'react';
import { shallow } from 'enzyme';
import ShowOverlayProvider from '../../../components/showOverlay/Provider';
import contextMock from '../../../mocks/showOverlay';

const { state, actions } = contextMock;

describe('ShowOverlay behavior tests', () => {
  let provider;
  beforeEach(() => {
    provider = shallow(
      <ShowOverlayProvider />
    );
  });

  it('should render the ShowOverlay', () => {
    expect(provider.find('.test-render').exists()).toBe(true);
  });

  it('should fetch the state to ShowOverlay', async () => {
    await provider.instance().actions.fetch().then(() => {
      expect(provider.state()).toEqual(state);
    });
  });

  it('should tick Tabs of ShowOverlay page', async () => {
    await provider.instance().actions.tick();
    expect(provider.state().start).toEqual(10);
  });

  it('should open an episode of ShowOverlay page', async () => {
    const ep = state.data.episodes[0];
    await provider.instance().actions.openEpisode(ep);
    expect(provider.state().episodeSelected).toEqual(ep);
  });
});
