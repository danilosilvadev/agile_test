import styled from 'styled-components';

export const PrimaryBackground = styled.div`
  color: ${({ theme }) => theme.primary.backgroundColor || 'black'};
`;

export const SecondaryBackground = styled.div`
  color: ${({ theme }) => theme.secondary.backgroundColor || 'black'};
  `;

export const TertiaryBackground = styled.div`
  color: ${({ theme }) => theme.tertiary.backgroundColor || 'black'};
`;
