import {
  MOBILE_SIZE,
  TABLET_SIZE,
  DESKTOP_SIZE,
  TV_SIZE,
} from './constantTypes';

const handleScreenSize = (screenSize) => {
  const tablet = 768;
  const desktop = 1200;
  const tv = 1440;
  switch (true) {
    case (screenSize <= tablet):
      return MOBILE_SIZE;
    case (screenSize > tablet && screenSize <= desktop):
      return TABLET_SIZE;
    case (screenSize < tv):
      return DESKTOP_SIZE;
    default:
      return TV_SIZE;
  }
};

export default handleScreenSize;
