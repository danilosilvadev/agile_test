const getSeasons = ep => (
  ep.reduce((final, current) => {
    if (current !== null && current.EpisodeNumber === 1) {
      final.push([current]);
    } else if (current !== null) {
      final[final.length - 1].push(current);
    }
    return final;
  }, [])
);

export default getSeasons;
