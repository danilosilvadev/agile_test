import axios from 'axios';

const BASE_URL = 'https://sample-api-78c77.firebaseio.com';
axios.defaults.baseURL = BASE_URL;

export default {
  show: showId => axios.all([
    axios.get(`/tv-shows/${showId}.json`),
    axios.get(`/episodes/${showId}.json`),
  ]).then(axios.spread((info, episodes) => ({
    episodes: episodes.data,
    info: info.data,
  }))),
};
