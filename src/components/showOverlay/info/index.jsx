import React from 'react';
import _ from 'lodash';
import Tabs, { TabPane } from 'rc-tabs';
import TabContent from 'rc-tabs/lib/TabContent';
import ScrollableInkTabBar from 'rc-tabs/lib/ScrollableInkTabBar';
import ReactSVG from 'react-svg';
import addTomyListIcons from '../../../assets/icons/add-gray-s.svg';
import evaluateIcon from '../../../assets/icons/sad-gray-w.svg';
import recordIcon from '../../../assets/icons/rec-gray-s.svg';
import shareIcon from '../../../assets/icons/share-gray-s.svg';
import './info.scss';

export default function Info({
  state: {
    data,
  },
}) {
  return (
    <div className='p-2 p-left-5 p-right-5 ag-info'>
      <Tabs
        defaultActiveKey='1'
        destroyInactiveTabPane
        renderTabBar={() => <ScrollableInkTabBar />}
        renderTabContent={() => <TabContent />}
      >
        <TabPane tab='GENERAL' key='1' placeholder='loading 1' className='f'>
          <ul className='f m-top-2 ag-info__icons-list'>
            <li>
              <ReactSVG src={addTomyListIcons} />
              <div className='ag-info__icons-text'>
                Mi Lista
              </div>
            </li>
            <li>
              <ReactSVG src={evaluateIcon} />
              <div className='ag-info__icons-text'>
                Evaluar
              </div>
            </li>
            <li>
              <ReactSVG src={recordIcon} />
              <div className='ag-info__icons-text'>
                Grabar
              </div>
            </li>
            <li>
              <ReactSVG src={shareIcon} />
              <div className='ag-info__icons-text'>
                Compartilhar
              </div>
            </li>
          </ul>
          <article className='f f-column m-top-2'>
            <div>
              SINOPSE
            </div>
            <div>
              {!(_.isEmpty(data)) && data.info.Synopsis}
            </div>
          </article>
        </TabPane>
        <TabPane tab='ELENCO' key='2' placeholder='loading 2'>
          <ul className='f m-2'>
            {!(_.isEmpty(data)) && data.info.Cast.map(cast => (
              <li key={cast.ID} className='p-left-6 p-right-6 p-top-3 p-bottom-3 ag-container__info-cast m-1'>
                {cast.Name}
              </li>
            ))}
          </ul>
        </TabPane>
        <TabPane tab='PRINCIPALES PREMIOS' key='3'>
          <div>PRẼMIOS MOCK</div>
        </TabPane>
      </Tabs>
    </div>
  );
}
