import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import Season from './seasonTabs';
import Info from './info';
import 'rc-tabs/assets/index.css';
import './tabsStyle.scss';
import './showOverlay.scss';

export default function Container({
  state,
  state: {
    data: {
      info,
    },
  },
  actions,
}) {
  return (
    <div className='ag-container'>
      {!(_.isEmpty(info)) && (
      <div>
        <BackgroundContainer background={info.Images.Background} className='ag-container__background-container f f-justify-between'>
          <div className='ag-container__overlay' />
          <section className='f f-column p-3 c-white ag-container__header'>
            <span className='ag-container__title'>
              {info.Title}
            </span>
            <span>
              {
                `80 % INDICADO / CIENCIA FICCIÓN /
                ${!(_.isEmpty(info)) && info.Year}
                / EUA / 14`
              }
            </span>
          </section>
          <div className='ag-container__seasons m-top-10 m-right-6 p-right-2'>
            <Season actions={actions} state={state} />
          </div>
        </BackgroundContainer>
        <div>
          <Info state={state} />
        </div>
      </div>
      )}
    </div>
  );
}

const BackgroundContainer = styled.div`
  background-image: ${({ background }) => `url(${background})`};
`;
