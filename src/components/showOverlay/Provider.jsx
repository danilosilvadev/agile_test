/* eslint-disable */

import React, { Component } from 'react';
import Container from './Container';
import api from '../../api';

class Provider extends Component {
  constructor() {
    super();
    this.state = {
      data: {},
      start: 0,
      episodeSelected: {},
    };
    this.actions = {
      tick: this.handleTick = this.handleTick.bind(this),
      openEpisode: this.handleOpenEpisode = this.handleOpenEpisode.bind(this),
      fetch: this.handleFetch = this.handleFetch.bind(this),
    };
  }

  componentDidMount() {
    this.actions.fetch();
  }

  handleFetch() {
    return api.show('SHOW123').then((data) => this.setState({ data }));
  }

  handleTick() {
    this.setState({
      start: this.state.start + 10,
    });
  }

  handleOpenEpisode(ep) {
    if (ep === this.state.episodeSelected) {
      this.setState({ episodeSelected: {} });
    } else {
      this.setState({ episodeSelected: ep });
    }
  }

  componentDidCatch(error, info) {
    console.log(error, info);
  }

  render() {
    return (
      <React.Fragment>
        <Container state={this.state} actions={this.actions} className='test-render' />
      </React.Fragment>
    );
  }
}

export default Provider;
