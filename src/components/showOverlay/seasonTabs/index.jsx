import React from 'react';
import _ from 'lodash';
import Tabs, { TabPane } from 'rc-tabs';
import styled, { keyframes } from 'styled-components';
import TabContent from 'rc-tabs/lib/TabContent';
import ScrollableInkTabBar from 'rc-tabs/lib/ScrollableInkTabBar';
import ReactSVG from 'react-svg';
import keyGen from '../../../utils/js/keyGenerator';
import getSeasons from '../../../utils/js/formatSeasons';
import './seasonTabs.scss';
import playIcon from '../../../assets/icons/play-small-player-w.svg';

export default function SeasonTabs({
  state: {
    data,
    episodeSelected,
  },
  actions: {
    openEpisode,
  },
}) {
  const seasons = data.episodes && getSeasons(data.episodes);
  return (
    <React.Fragment>
      <Tabs
        defaultActiveKey='1'
        destroyInactiveTabPane
        renderTabBar={() => <ScrollableInkTabBar />}
        renderTabContent={() => <TabContent />}
      >
        {
          data.episodes && seasons.map(season => (
            <TabPane tab={`T${season[0].SeasonNumber}`} key={season[0].SeasonNumber} placeholder='loading 1' className='f f-column align-end justify-end'>
              {!(_.isEmpty(data)) && (
                season
              ).map(ep => (
                <section key={keyGen()} className='ag-season-tabs__episode'>
                  <div className='f f-align-center f-justify-between'>
                    <button type='button' onClick={() => openEpisode(ep)} className='ag-season-tabs__episode p-top-2 p-bottom-2' style={{ borderBottom: episodeSelected === ep ? '0' : '1px solid white' }}>
                      {`${(ep && ep.EpisodeNumber) || 'episode number required'}
                      ${(ep && ep.Title) || 'episode info required'}`}
                    </button>
                    <div>
                      <ReactSVG src={playIcon} svgClassName='ag-season-tabs__play-icon' />
                    </div>
                  </div>
                  { episodeSelected === ep ? (
                    <ThumbShow className='m-right-4 m-bottom-1'>
                      <img src={ep.Image} alt='thumb' className='m-bottom-1' />
                      {ep.Synopsis}
                    </ThumbShow>
                  ) : null}
                </section>
              ))}
            </TabPane>
          ))
        }
      </Tabs>
    </React.Fragment>
  );
}

const fadeIn = keyframes`
  0% {
    height: 0;
    opacity: 0;
  }
  100% {
    height: 100%;
    opacity: 1;
  }
`;

const ThumbShow = styled.div`
  animation: .3s ${fadeIn} ease-out;
`;
